# LearnEnglish

#### 介绍
学习英语-主要以程序员单词为主
英文与中文五笔打字练习

#### 统计工具使用案例

```java
StopWatch sw = new StopWatch();
    
        // 待处理文件或文件夹
        String folder = "C:\\Users\\Administrator\\Desktop\\常用.txt";

        sw.start();
        wordScanner = WordScanner.Builder()
                .withRootDir(folder)
                // 设置保存路径或保存文件名
                .withFileWillSave("WubiCountWordList.txt")
                // 只处理指定的文件
                .withFileFilter(f -> true)
                // 文本或单词处理策略
                .withWordStrategy(new ChineseWubiStrategy())
                // 排序规则：单词字母、词频、难度排序等
                .withDefaultComparator(WordInfo.CountComparator)
                .build();
        wordScanner.scan().save();
        sw.stop();
    
        String s = sw.prettyPrint();
        System.out.println(s);
```

结果如下:
```log
我                             	 70846	0.29	E
了                             	 68623	0.33	D
是                             	 50841	0.17	F
图片                           	 42490	0.42	C
你                             	 38333	0.58	B
北京                             33930	0.38	D
上海                           	 31968	0.38	D
不                             	 24335	0.50	B
就                             	 20899	0.42	C
有                             	 20267	0.50	B
啊                             	 20077	0.50	B
吗                             	 16273	0.44	C
也                             	 14792	0.33	D
在                             	 14504	0.38	D
杭州                           	 14204	0.42	C
好                             	 13211	0.28	E
一个                           	 12252	0.42	C
可以                           	 10852	0.46	C
没                             	 10828	0.38	D
吧                             	 10763	0.44	C
消息                           	 10588	0.42	C
想                             	 9994	0.33	D
什么                           	 9474	0.42	C
就是                           	 9091	0.29	E
浮世                           	 8771	0.43	C
不是                           	 8693	0.29	E
怎么                           	 8660	0.38	D
做                             	 8433	0.46	C
去                             	 8324	0.22	E
个                             	 8219	0.33	D
```