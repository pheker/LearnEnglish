package cn.pheker.learnenglish.lookingforwords;

import java.util.List;
import java.util.function.Consumer;

/**
 * author    version  date
 * ---------------------------------
 * cn.pheker v1.0.0   2019/4/7 14:21
 *
 * @Description 单词本接口
 */
public interface Book {
    
    /**
     * Word数量
     * @return
     */
    int size();
    
    /**
     * 添加新Word
     *
     * @param word
     * @return
     */
    Word add(String word);
    
    
    /**
     * 前缀查询Word 分批返回数据 每次返回的数量sizeFor
     * @param prefix
     * @param sizePer 设置 每次返回的数量sizeFor 最小为2
     * @param consumer
     */
    void searchWithPrefix(String prefix, int sizePer, Consumer<List<Word>> consumer);
    
    /**
     * 通过前缀查询单词
     * @param prefix 前缀
     * @return 前缀是prefix的所有单词
     */
    List<Word> searchWithPrefix(String prefix);
    
    /**
     * 通过前缀查询单词
     * @param prefix 前缀
     * @return
     */
    void searchWithPrefix(String prefix, Consumer<Word> consumer);
    
    /**
     * 是否包含前缀
     * @param prefix 前缀
     * @return
     */
    boolean contains(String prefix);
    
    /**
     * 查询单个Word--通过前缀查询实现
     *
     * @param word
     * @return
     */
    Word search(String word);
    
    /**
     * 查询所有Word--通过前缀查询实现
     *
     * @return
     */
    List<Word> list();
    
    /**
     * 前缀查询Word 分批返回数据 每次返回的数量sizeFor
     * @param sizePer 设置 每次返回的数量sizeFor 最小为2
     * @param consumer 分批数据
     */
    void list(int sizePer, Consumer<List<Word>> consumer);
    
    
}
