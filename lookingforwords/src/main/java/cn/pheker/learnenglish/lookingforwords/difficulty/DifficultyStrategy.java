package cn.pheker.learnenglish.lookingforwords.difficulty;

import cn.pheker.learnenglish.lookingforwords.difficulty.Difficulty;

/**
 * <pre>
 * @author cn.pheker
 * @date 2020/5/4 12:25
 * @email 1176479642@qq.com
 * @version v1.0.0
 * @desc 单词难度策略
 *
 * </pre>
 */
public interface DifficultyStrategy {
    
    /**
     * 评估单词的平均难度级别
     * @param word 单词/字符串
     * @return
     */
    Difficulty evaluate(String word);
}
