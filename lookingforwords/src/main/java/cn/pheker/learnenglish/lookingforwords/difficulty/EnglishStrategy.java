package cn.pheker.learnenglish.lookingforwords.difficulty;

import cn.pheker.learnenglish.lookingforwords.util.UtilRegexp;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;

/**
 * <pre>
 * @author cn.pheker
 * @date 2020/5/4 12:38
 * @email 1176479642@qq.com
 * @version v1.0.0
 * @desc 英文单词难度策略
 *
 * </pre>
 */
public class EnglishStrategy implements WordStrategy {
    /**
     * 难度级别名称, 共分九级：F最低，SSS最高
     */
    List<String> level = Arrays.asList("F", "E", "D", "C", "B", "A", "S", "SS", "SSS");
    
    @Override
    public Difficulty evaluate(String word) {
        Double score = getSumScore(word);
        int levelIdx = (int) (score*10) -1;
        levelIdx = levelIdx < 0 ? 0 : levelIdx > 8 ? 8 : levelIdx;
        return Difficulty.of(score, level.get(levelIdx));
    }
    
    public static List<String> score5 = Arrays.asList("Q","A","Z","P","'");
    public static List<String> score4 = Arrays.asList("W","S","X","O","L");
    public static List<String> score3 = Arrays.asList("E","D","C","I","K");
    public static List<String> score2 = Arrays.asList("T","G","B","Y","H","N");
    public static List<String> score1 = Arrays.asList("R","F","V","Y","H","M");
    
    public static Double getSingleScore(char ch){
        String c = (ch+"").toUpperCase();
        Double score = 0d;
        if (score5.contains(c)) {
            score = 5.0;
        }else if (score4.contains(c)) {
            score = 4.0;
        }else if (score3.contains(c)) {
            score =  3.0;
        }else if (score2.contains(c)) {
            score =  2.0;
        }else if (score1.contains(c)) {
            score =  1.0;
        }
        if (Character.isUpperCase(ch)) {
            score++;
        }
        return score;
    }
    
    public static Double getSumScore(String word) {
        double sum = 0.0;
        int len = word.length();
        for (int i = 0; i < len; i++) {
            sum += getSingleScore(word.charAt(i));
        }
        BigDecimal bigDecimal = new BigDecimal(sum/(6.0*len));
        return bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }
    
    @Override
    public List<String> search(String str) {
        List<String> words = new ArrayList<String>();
        Matcher matcher = UtilRegexp.PATTERN_WORD_ENGLISH.matcher(str);
        while (matcher.find()) {
            String group = matcher.group();
            if(group.contains("_") || UtilRegexp.containUpperCase(group)) {
                String[] split = group.split("_|(?=[A-Z])");
                int length = split.length;
                for (int i = 0; i < length; i++) {
                    String w = split[i];
                    if (UtilRegexp.isNullOrEmpty(w)
                            || w.length() < 2
                            || w.length() > 20
                            || UtilRegexp.containNumbers(w)) {
                        continue;
                    }
                    words.add(split[i].toLowerCase());
                }
            }
        }
        return words;
    }
}
