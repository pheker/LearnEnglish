package cn.pheker.learnenglish.lookingforwords;

import cn.pheker.learnenglish.lookingforwords.difficulty.EnglishStrategy;
import cn.pheker.learnenglish.lookingforwords.difficulty.WordStrategy;
import cn.pheker.learnenglish.lookingforwords.util.UtilFile;
import cn.pheker.learnenglish.lookingforwords.util.UtilRegexp;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * <pre>
 * @author    cn.pheker
 * @date      2018/3/30 10:45
 * @email     1176479642@qq.com
 * @desc      扫描指定路径中所有java或kotlin中的单词
 *
 * </pre>
 */
@Slf4j
public class WordScanner {

    /**
     * 要保存的目录或文件， 如果是文件的话会被解析成rootDir目录与fileWillSave文件名
     */
    private String rootDir;
    /**
     * 要保存的文件名或全路径名
     * 1.如果是文件名的话则会保存到rootDir
     * 2.如果是全路径名的话则直接保存到这个位置
     */
    private String fileWillSave;
    /**
     * 要扫描的文件夹或文件
     */
    private List<String> scanFiles;
    
    /**
     * 已处理的文件数量
     */
    private AtomicInteger cntFile = new AtomicInteger();
    
    /**
     * 要查找的文件： 默认为java文件
     */
    private FileFilter fileFilter;
    
    private Predicate<String> wordFilter;
    /**
     * 单词比较器：用于保存时排序
     */
    private Comparator<Word> defaultComparator = Word.WordComparator;
    
    /**
     * 单词查找与难度计算策略
     */
    private WordStrategy wordStrategy;
    
    /**
     * 分批保存单词， 默认1000， 最小为2
     */
    private int sizePer;
    
    private ThreadPoolExecutor tp = new ThreadPoolExecutor(5, 20, 30, TimeUnit.SECONDS,
            new LinkedBlockingQueue<>(10),
            new ThreadPoolExecutor.CallerRunsPolicy());
    private ReentrantLock lock = new ReentrantLock();
    private final int unParseMax = 10;
    private CopyOnWriteArraySet<File>  unParseFiles = new CopyOnWriteArraySet<>();
    
    private WordBook book;
    
    public static Builder Builder() {
        return Builder.create();
    }
    public Saver Saver(){
        return Saver.create(this);
    }
    
    /**
     * 要保存单词到的文件或文件夹路径
     * @param rootDir
     */
    public WordScanner(String rootDir) {
        this.rootDir = rootDir;
        File root = new File(rootDir);
        if (!root.exists()) {
            throw new RuntimeException("文件不存在: "+rootDir);
        }
        if (root.isFile()) {
            this.rootDir = root.getParent();
            this.fileWillSave = root.getName();
        }
    
        if (this.rootDir.contains("\\")) {
            this.rootDir = this.rootDir.replaceAll("\\\\+", "/");
        }
    }
    
    /**
     * 扫描并保存到当前文件夹
     * @param rootDir 要扫描的文件或文件夹
     */
    public static void scanAndSave(String rootDir) {
        WordScanner wordScanner = WordScanner.Builder()
                .withRootDir(rootDir)
                .withAddExtraScanFiles(rootDir)
                .build();
        wordScanner.scanAndSave();
    }
    
    public void scanAndSave() {
        this.scan().save();
    }
    
    public WordScanner scan(){
        // 防止重复扫描
        if(book == null) {
            book = WordBook.build(this.wordStrategy);
            if (scanFiles == null || scanFiles.isEmpty()) {
                throw new RuntimeException("Don't specify the scanFiles with the method withAddExtraScanFiles");
            }
            scanFiles.forEach(f -> scanFilesWithThreadPool(new File(f)));
            // 每unParseMax个文件或剩余的文件开启一个线程处理
            if(!unParseFiles.isEmpty()){
                List<File> finalFiles = Arrays.asList(unParseFiles.toArray(new File[0]));
                tp.submit(() -> parseFiles(finalFiles));
            }
            tp.shutdown();
            
            // 最多等待10秒终止扫描
            try {
                tp.awaitTermination(10, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                log.error("扫描文件过多:{}", cntFile);
            }
        }
        return this;
    }
    
    /**
     * 多线程递归扫描并处理文件
     * @param f
     */
    private void scanFilesWithThreadPool(File f) {
        if(f == null || !f.exists()) {
            return;
        }
        if (f.isFile() && fileFilter.accept(f)) {
            List<File> files = null;
            lock.lock();
            try {
                if (unParseFiles.size() < unParseMax) {
                    unParseFiles.add(f);
                } else {
                    files = Arrays.asList(unParseFiles.toArray(new File[0]));
                    unParseFiles.clear();
                }
            }finally {
                lock.unlock();
            }
            if(files != null){
                // 每unParseMax个文件开启一个线程处理
                List<File> finalFiles = files;
                tp.submit(() -> parseFiles(finalFiles));
            }
        }else{
            File[] files = f.listFiles();
            if(files != null) {
                for (File file : files) {
                    scanFilesWithThreadPool(file);
                }
            }
        }
    }
    
    private void parseFiles(List<File> finalFiles) {
        finalFiles.stream()
                .flatMap(ff -> {
                    if(log.isDebugEnabled()) {
                        log.debug("[正在解析]: {}", ff.getAbsolutePath());
                    }
                    String content = UtilFile.readLogByString(ff);
                    List<String> words = book.getStrategy().search(content);
                    return words.stream();
                })
                .distinct()
                .filter(wordFilter)
                .forEach(word -> {
                    book.add(word);
                });
        cntFile.addAndGet(unParseMax);
    }
    
    public String getRootDir() {
        return rootDir;
    }
    
    public AtomicInteger getCntFile() {
        return cntFile;
    }
    
    public Book getBook() {
        return book;
    }

    public void save() {
        this.Saver().save();
    }
    
    public void saveAsJson() {
        this.Saver().saveAsJson();
    }
    
    /**
     * 是否保存为json
     * @param json
     */
    private void saveAndPrint(boolean json) {
        String savePath = fileWillSave.contains("/") ? fileWillSave : rootDir + "/" + fileWillSave;
        File file = new File(savePath);
    
        if(json) {
            UtilFile.writeString(file, "[\n", false);
        }
        StringBuilder wordList = new StringBuilder();
        List<Word> list = this.book.list().stream()
                .sorted(this.defaultComparator)
                .collect(Collectors.toList());
        int size = list.size(), cnt = 0, times = 0;
        if (size > 0) {
            log.debug("[正在保存单词]");
        }
        boolean append = false;
        for (int i = 0; i < size; i++) {
            Word word = list.get(i);
            String s = word == null ? "" : (json ?
                    word.toJsonFormat() + ","
                    : word.toStringFormat()) + "\n";
            wordList.append(s);
            cnt++;
            if (cnt >= this.sizePer) {
                if ( i == size -1 && json) {
                    int len = wordList.length();
                    wordList.delete(len-2, len).append("\n]");
                }
                UtilFile.writeString(file, wordList.toString(), append);
                log.debug("[已保存单词数量]: {}", cnt * (++times));
                if (!append) {
                    append = true;
                }
                
                // 重置
                wordList = new StringBuilder();
                cnt = 0;
            }
        }
        int len = 0;
        if (json && (len = wordList.length()) > 0) {
            wordList.delete(len-2, len).append("\n]");
        }
        if(wordList.length() > 0) {
            UtilFile.writeString(file, wordList.toString(), append);
            log.debug("[已保存单词数量]: {}", size);
        }
        log.info("文件总数量: {}", cntFile);
        log.info("单词总数量: {}", book.size());
        log.info("单词  数量: {}", size);
        log.info("单词本已保存: {}", savePath);
    }
    
    
    public static final class Builder {
        /**
         * 要保存到的目录
         */
        private String rootDir;
        /**
         * 要扫描的文件或文件夹
         */
        private List<String> scanFiles = new ArrayList<>();
        /**
         * 要保存到哪个文件， 默认文件名为WordList.txt, 默认文件夹为rootDir
         */
        private String fileWillSave = "WordList.txt";
        private FileFilter fileFilter = (File f) -> {
            if(f.isDirectory()) {
                return false;
            }
            String path = f.getAbsolutePath();
            String ext = UtilFile.getFileExt(path).toLowerCase();
            return !UtilRegexp.isNullOrEmpty(ext) && ext.endsWith(".java");
        };
        private Predicate<String> wordFilter = word -> word != null && word.length()>0;
        private Comparator<Word> defaultComparator = Word.WordComparator;
        /**
         * 单词查找与难度计算策略
         */
        private WordStrategy wordStrategy = new EnglishStrategy();
        
        private Builder() {
        }
        
        public static Builder create() {
            return new Builder();
        }
        
        public Builder withRootDir(String rootDir) {
            this.rootDir = rootDir;
            return this;
        }
    
        public Builder withFileWillSave(String fileWillSave) {
            this.fileWillSave = fileWillSave.replaceAll("\\\\+", "/");
            return this;
        }
        public Builder withFileFilter(FileFilter fileFilter) {
            if (fileFilter != null) {
                this.fileFilter = fileFilter;
            }
            return this;
        }
    
        public Builder withWordFilter(Predicate<String> wordFilter) {
            if (wordFilter != null) {
                this.wordFilter = wordFilter;
            }
            return this;
        }
        
        public Builder withDefaultComparator(Comparator<Word> comparator) {
            if(comparator != null) {
                this.defaultComparator = comparator;
            }
            return this;
        }
        public Builder withWordStrategy(WordStrategy wordStrategy) {
            if(wordStrategy != null) {
                this.wordStrategy = wordStrategy;
            }
            return this;
        }
        public Builder withAddExtraScanFiles(String... extraScanFiles) {
            this.scanFiles.addAll(Arrays.asList(extraScanFiles));
            return this;
        }
        
        public WordScanner build() {
            WordScanner wordScanner = new WordScanner(rootDir);
            wordScanner.scanFiles = this.scanFiles;
            if (this.fileFilter != null) {
                wordScanner.fileFilter = this.fileFilter;
            }
            if (this.defaultComparator != null) {
                wordScanner.defaultComparator = this.defaultComparator;
            }
            if (this.wordFilter != null) {
                wordScanner.wordFilter = this.wordFilter;
            }
            wordScanner.fileWillSave = this.fileWillSave;
            if (this.wordStrategy != null) {
                wordScanner.wordStrategy = this.wordStrategy;
            }
            return wordScanner;
        }
    
    }
    
    
    public static final class Saver {
        private WordScanner wordScanner;
        /**
         * 要保存到哪个文件， 默认文件名为WordList.txt, 默认文件夹为rootDir
         */
        private String fileWillSave;
        private Comparator<Word> defaultComparator;
        /**
         * 每次保存单词的数量, 最小为2
         */
        private int sizePer = 1000;
        
        private Saver(WordScanner wordScanner) {
            this.wordScanner = wordScanner;
        }
        
        public static Saver create(WordScanner wordScanner) {
            return new Saver(wordScanner);
        }
        
        public Saver withFileWillSave(String fileWillSave) {
            this.fileWillSave = fileWillSave.replaceAll("\\\\+", "/");
            return this;
        }
        
        public Saver withDefaultComparator(Comparator<Word> defaultComparator) {
            if(defaultComparator != null) {
                this.defaultComparator = defaultComparator;
            }
            return this;
        }
        
        public Saver withSizePer(int sizePer){
            if (sizePer < 2) {
                sizePer = 2;
            }
            this.sizePer = sizePer;
            return this;
        }
        
        public void save() {
            if(defaultComparator != null) {
                wordScanner.defaultComparator = this.defaultComparator;
            }
            if(this.fileWillSave != null) {
                wordScanner.fileWillSave = this.fileWillSave;
            }
            wordScanner.sizePer = this.sizePer;
            wordScanner.saveAndPrint(false);
        }
        public void saveAsJson() {
            wordScanner.defaultComparator = this.defaultComparator;
            wordScanner.fileWillSave = this.fileWillSave;
            wordScanner.sizePer = this.sizePer;
            wordScanner.saveAndPrint(true);
        }
    }
}
