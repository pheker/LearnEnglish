package cn.pheker.learnenglish.lookingforwords.common;

import cn.pheker.learnenglish.lookingforwords.WordBook;
import cn.pheker.learnenglish.lookingforwords.Word;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * <pre>
 * @author cn.pheker
 * @date 2020/5/23 20:57
 * @email 1176479642@qq.com
 * @version v1.0.0
 * @desc 节点类
 *单词信息:节点树中的最后一个节点
 *如： 中->
 *      国->
 *          人
 * </pre>
 */
@Slf4j
@Data
public class Node{
    public static final int ROOT_CODE = -1;
    WordBook wordBook;
    int code;
    int deep;
    transient Node pNode;
    
    public Node() {
    }
    
    public Node(WordBook wordBook, int code) {
        this.wordBook = wordBook;
        this.code = code;
    }
    
    Map<Integer, Node> children;
    
    public Node(WordBook wordBook, int code, int deep, Node pNode) {
        this.wordBook = wordBook;
        this.code = code;
        this.deep = deep;
        this.pNode = pNode;
    }
    
    /**
     * 判断存在后续节点
     */
    public boolean hasChildren() {
        return children != null && !children.isEmpty();
    }
    
    /**
     * 是否是根节点(根节点code = -1)
     */
    public boolean isRoot() {
        return code == ROOT_CODE;
    }
    
    /**
     * 是否是单词节点
     * @return
     */
    public boolean isWordNode() {
        return this instanceof WordNode;
    }
    /**
     * 解析字符串(不为空)
     * @param word
     * @return
     */
    public Word parseWord(String word) {
        int index = 0, wordLen = word.length();
        // next cursor..
        WordNode wordNode = (WordNode) addChild(word, wordLen, index);
        
        return Word.parseWord(wordNode);
    }
    
    public Node getPNode() {
        return pNode;
    }
    
    /**
     * 递归解析word的每一部分 并添加当前节点中作为子节点
     * @param word
     * @param wordLen
     * @param index
     * @return
     */
    private synchronized Node addChild(String word, int wordLen, int index) {
        wordBook.updateMaxDeep(wordLen);
        if (index < wordLen) {
            int code = word.codePointAt(index);
            //无子节点
            if (!hasChildren()) {
                children = new HashMap<>(4);
            }
            
            //是否存在对应的子节点
            Node child = children.get(code);
            if (child == null){
                if (index < wordLen-1) {
                    child = new Node(wordBook, code, deep+1, this);
                }else{
                    child = new WordNode(wordBook, code, deep + 1, this);
                    wordBook.incrSize();
                }
                children.put(code, child);
            }
            return child.addChild(word, wordLen, ++index);
        }
        return this.transferToInfoNode(word);
    }
    
    /**
     * 将当前节点转换成InfoNode节点,此节点会保存单词信息
     * @param word
     * @return
     */
    private WordNode transferToInfoNode(String word) {
        WordNode wordNode = null;
        if (this instanceof WordNode) {
            wordNode = ((WordNode) this);
            // 普通节点需要转换为InfoNode
        }else {
            wordNode = new WordNode(wordBook, this.code, this.deep, this.pNode);
            WordNode finalWordNode = wordNode;
            
            // 自己作为子节点的父节点
            wordNode.children = this.children.keySet().stream()
                    .collect(Collectors.toMap(k -> k, k->{
                        Node subNode = this.children.get(k);
                        subNode.pNode = finalWordNode;
                        return subNode;
                    }));
            
            //并把自己放入父节点中
            this.pNode.children.put(code, wordNode);
        }
        wordNode.incrCount();
        if (wordNode.getCount() == 1 && log.isDebugEnabled()) {
            log.debug("[add word]: {}", word);
        }
        return wordNode;
    }
    
    private void search(StringBuilder word, Consumer<Word> consumer) {
        if (this instanceof WordNode) {
            Word wordInfo = new Word(word.toString(), null);
            consumer.accept(wordInfo);
        }else{
            this.children.values().forEach(child -> {
                child.search(word, consumer);
            });
        }
        
    }
    
    public Node findPrefixNode(String prefix, int len, int i) {
        if (i < len) {
            if (!hasChildren()) {
                return null;
            }
            Collection<Node> nodes = this.children.values();
            Iterator<Node> nodeIte = nodes.iterator();
            if(isRoot()){
                for (Node node: nodes) {
                    Node nodeFound = node.findPrefixNode(prefix, len, i);
                    if (nodeFound != null) {
                        return nodeFound;
                    }
                }
            }else if(prefix.charAt(i) == code){
                i++;
                if (i == len) {
                    return this;
                }
                for (Node node: nodes) {
                    Node nodeFound = node.findPrefixNode(prefix, len, i);
                    if (nodeFound != null) {
                        return nodeFound;
                    }
                }
            }
        }
        // not found
        return null;
    }
    
    public String getWord() {
        if (isRoot()) {
            return null;
        }
        StringBuilder word = new StringBuilder();
        Node node = this;
        while (!node.isRoot()) {
            word.insert(0, Character.toChars(node.code));
            node = node.pNode;
        }
        return word.toString();
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Node node = (Node) o;
        return code == node.code &&
                deep == node.deep;
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(code, deep);
    }
    
    @Override
    public String toString() {
        return String.format("{\"code\":\"%s\", \"deep\":%d}", (char)code, deep);
    }
}

