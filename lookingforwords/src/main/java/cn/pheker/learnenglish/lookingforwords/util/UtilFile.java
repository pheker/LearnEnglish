package cn.pheker.learnenglish.lookingforwords.util;


import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * <pre>
 * @author    cn.pheker
 * @date      2018/3/30 10:56
 * @email     1176479642@qq.com
 * @desc      文件工具类
 *
 * </pre>
 */

public class UtilFile {
    
    private static String newline = "\n";
    
    /**
     * 写入文件,末尾自动添加\n
     * utf-8  追加
     *
     * @param path
     * @param str
     */
    public static void writeLog(String path, String str) {
        FileOutputStream out = null;
        try {
            File file = new File(path);
            if (!file.exists()) {
                file.createNewFile();
            }
            //true表示追加
            out = new FileOutputStream(file);
            StringBuffer sb = new StringBuffer();
            sb.append(str + newline);
            out.write(sb.toString().getBytes("utf-8"));
        } catch (IOException ex) {
            System.out.println(ex.getStackTrace());
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    /**
     * 写入文件,末尾自动添加\n
     *
     * @param path
     * @param str
     */
    public static void writeLog(String path, String str, boolean is_append, String encode) {
        try {
            File file = new File(path);
            if (!file.exists()) {
                file.createNewFile();
            }
            FileOutputStream out = new FileOutputStream(file, is_append); //true表示追加
            StringBuffer sb = new StringBuffer();
            sb.append(str + newline);
            out.write(sb.toString().getBytes(encode));//
            out.close();
        } catch (IOException ex) {
            System.out.println(ex.getStackTrace());
        }
    }
    
    /**
     * 整个文件以string放回，添加\r\n换行
     *
     * @param path
     * @return
     */
    public static String readLogByString(String path) {
        File file = new File(path);
        return readLogByString(file);
    }
    /**
     * 整个文件以string放回，添加\n换行
     *
     * @param file
     * @return
     */
    public static String readLogByString(File file) {
        StringBuffer sb = new StringBuffer();
        String tempstr = null;
        try {
            if (!file.exists()) {
                throw new FileNotFoundException();
            }
            FileInputStream fis = new FileInputStream(file);
            BufferedReader br = new BufferedReader(new InputStreamReader(fis, "utf-8"));
            while ((tempstr = br.readLine()) != null) {
                sb.append(tempstr + "\r\n");
            }
        } catch (IOException ex) {
            System.out.println(ex.getStackTrace());
        }
        return sb.toString();
    }
    
    
    /**
     * 加入编码
     * 整个文件以string放回，添加\n换行
     *
     * @param path
     * @return
     */
    public static String readLogByStringAndEncode(String path, String encode) {
        StringBuffer sb = new StringBuffer();
        String tempstr = null;
        try {
            File file = new File(path);
            if (!file.exists()) {
                throw new FileNotFoundException();
            }
            FileInputStream fis = new FileInputStream(file);
            BufferedReader br = new BufferedReader(new InputStreamReader(fis, encode));
            while ((tempstr = br.readLine()) != null) {
                sb.append(tempstr + newline);
            }
        } catch (IOException ex) {
            System.out.println(ex.getStackTrace());
        }
        return sb.toString();
    }
    
    /**
     * 按行读取文件，以list<String>的形式返回
     *
     * @param path
     * @return
     */
    public static List<String> readLogByList(String path) {
        List<String> lines = new ArrayList<String>();
        String tempstr = null;
        try {
            File file = new File(path);
            if (!file.exists()) {
                throw new FileNotFoundException();
            }
            FileInputStream fis = new FileInputStream(file);
            BufferedReader br = new BufferedReader(new InputStreamReader(fis, "utf-8"));
            while ((tempstr = br.readLine()) != null) {
                lines.add(tempstr.toString());
            }
        } catch (IOException ex) {
            System.out.println(ex.getStackTrace());
        }
        return lines;
    }
    
    
    /**
     * 创建目录
     *
     * @param dir_path
     */
    public static void mkDir(String dir_path) {
        File myFolderPath = new File(dir_path);
        try {
            if (!myFolderPath.exists()) {
                myFolderPath.mkdir();
            }
        } catch (Exception e) {
            System.out.println("新建目录操作出错");
            e.printStackTrace();
        }
    }
    
    /**
     * 创建文件
     *
     * @param file_path
     */
    public static void createNewFile(String file_path) {
        File myFilePath = new File(file_path);
        try {
            if (!myFilePath.exists()) {
                myFilePath.createNewFile();
            }
        } catch (Exception e) {
            System.out.println("新建文件操作出错");
            e.printStackTrace();
        }
    }
    
    /**
     * 递归删除文件或者目录
     *
     * @param file_path
     */
    public static void deleteEveryThing(String file_path) {
        try {
            File file = new File(file_path);
            if (!file.exists()) {
                return;
            }
            if (file.isFile()) {
                file.delete();
            } else {
                File[] files = file.listFiles();
                for (int i = 0; i < files.length; i++) {
                    String root = files[i].getAbsolutePath();//得到子文件或文件夹的绝对路径
                    deleteEveryThing(root);
                }
                file.delete();
            }
        } catch (Exception e) {
            System.out.println("删除文件失败");
        }
    }
    
    /**
     * 得到一个文件夹下所有文件
     */
    public static List<String> getAllFileNameInFold(String fold_path) {
        List<String> file_paths = new ArrayList<String>();
        
        LinkedList<String> folderList = new LinkedList<String>();
        folderList.add(fold_path);
        while (folderList.size() > 0) {
            File file = new File(folderList.peekLast());
            folderList.removeLast();
            File[] files = file.listFiles();
            ArrayList<File> fileList = new ArrayList<File>();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    folderList.add(files[i].getPath());
                } else {
                    fileList.add(files[i]);
                }
            }
            for (File f : fileList) {
                file_paths.add(f.getAbsoluteFile().getPath());
            }
        }
        return file_paths;
    }
    
    public static String getFileExt(String name) {
        int i = 0;
        if (UtilRegexp.isNullOrEmpty(name)) {
            return "";
        } else if ((i = name.lastIndexOf(".")) == -1) {
            return "";
        }
        return name.substring(i);
    }
    
    public static String getFileName(String path) {
        return path.substring(path.lastIndexOf("\\") + 1);
    }
    
    /**
     * 写入文件
     *
     * @param file
     * @param content
     */
    public static void writeString(File file, String content, boolean append) {
        try {
            if (!file.exists()) {
                file.createNewFile();
            }
            //true表示追加
            FileOutputStream out = new FileOutputStream(file, append);
            out.write(content.getBytes("UTF-8"));
            out.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * 判断编码格式
     */
    public static  String getFileCharset(File sourceFile) {
        String charset = "GBK";
        byte[] first3Bytes = new byte[3];
        BufferedInputStream bis = null;
        try {
            boolean checked = false;
            bis = new BufferedInputStream(new FileInputStream(sourceFile));
            bis.mark(0);
            int read = bis.read(first3Bytes, 0, 3);
            // 文件编码为 ANSI
            if (read == -1) {
                return charset;
                // 文件编码为 Unicode
            } else if (first3Bytes[0] == (byte) 0xFF
                    && first3Bytes[1] == (byte) 0xFE) {
                charset = "UTF-16LE";
                checked = true;
                // 文件编码为 Unicode big endian
            } else if (first3Bytes[0] == (byte) 0xFE
                    && first3Bytes[1] == (byte) 0xFF) {
                charset = "UTF-16BE";
                checked = true;
                // 文件编码为 UTF-8
            } else if (first3Bytes[0] == (byte) 0xEF
                    && first3Bytes[1] == (byte) 0xBB
                    && first3Bytes[2] == (byte) 0xBF) {
                charset = "UTF-8";
                checked = true;
            }
            bis.reset();
            if (!checked) {
                int loc = 0;
                while ((read = bis.read()) != -1) {
                    loc++;
                    if (read >= 0xF0) {
                        break;
                    }
                    // 单独出现BF以下的，也算是GBK
                    if (0x80 <= read && read <= 0xBF) {
                        break;
                    }
                    if (0xC0 <= read && read <= 0xDF) {
                        read = bis.read();
                        // 双字节 (0xC0 - 0xDF)
                        if (0x80 <= read && read <= 0xBF) {
                            // (0x80
                            // - 0xBF),也可能在GB编码内
                            continue;
                        } else {
                            break;
                        }
                        // 也有可能出错，但是几率较小
                    } else if (0xE0 <= read && read <= 0xEF) {
                        read = bis.read();
                        if (0x80 <= read && read <= 0xBF) {
                            read = bis.read();
                            if (0x80 <= read && read <= 0xBF) {
                                charset = "UTF-8";
                                break;
                            } else {
                                break;
                            }
                        } else {
                            break;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (bis != null) {
                try {
                    bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return charset;
    }
    
    /**
     * 获取当前执行程序或jar/exe路径
     */
    public static String getRealPath(Class clzz) {
        java.net.URL url = clzz.getProtectionDomain().getCodeSource().getLocation();
        String filePath = null;
        try {
            filePath = java.net.URLDecoder.decode(url.getPath(), "utf-8");
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        if (filePath.endsWith(".jar") || filePath.endsWith(".exe")) {
            filePath = filePath.substring(0, filePath.lastIndexOf("/") + 1);
        }
        File file = new File(filePath);
        
        filePath = file.getAbsolutePath();
        return filePath.replaceAll("\\\\", "/");
    }
    
    public static void main(String[] args) {
//        String path = "C:\\Users\\chenhuan001\\workspace\\CrawlSinaBySelenium\\src";
//        List<String> file_paths = getAllFileNameInFold(path);
//        for(String file_path : file_paths) {
//            System.out.println(file_path);
//        }
        deleteEveryThing("C:\\Users\\chenhuan001\\Desktop\\testDelete.txt");

//        List<Document> docs = readDocsFromFile("Data/user_program_data.txt");
//        System.out.println(docs.size());
//        for (int i = 0; i < docs.size(); i++) {
//            System.out.println(docs.toString());
//        }
        //mkDir("tmp_dir");
        //createNewFile("tmp_dir/new_file1.txt");
        //deleteEveryThing("save.arff");
    }
    
}