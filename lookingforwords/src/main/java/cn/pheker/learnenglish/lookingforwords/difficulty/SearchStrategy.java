package cn.pheker.learnenglish.lookingforwords.difficulty;

import java.util.List;

/**
 * <pre>
 * @author cn.pheker
 * @date 2020/5/4 19:51
 * @email 1176479642@qq.com
 * @version v1.0.0
 * @desc 查找策略, 从文本中找到符合的字符串
 *
 * </pre>
 */
public interface SearchStrategy {
    
    /**
     * 从文本中找到符合的字符串
     * @param str
     * @return
     */
    List<String> search(String str);
    
}
