package cn.pheker.learnenglish.lookingforwords.util;

/**
 * <pre>
 * @author cn.pheker
 * @date 2020/3/13 18:10
 * @email 1176479642@qq.com
 * @version v1.0.0
 * @desc 键值对信息类
 *
 * </pre>
 */
public class Span<K, V> {
    private final K k;
    private final V v;
    
    public static <K, V> Span ofNull(){
        return new Span(null, null);
    }
    
    public static <K, V> Span of(K k, V v) {
        return new Span(k, v);
    }
    private Span(K k, V v) {
        this.k = k;
        this.v = v;
    }
    
    /**
     * 将字符串分隔成Span
     * @param origin
     * @param splitPattern
     * @return
     */
    public static  Span<String, String> ofSpilit(String origin, String splitPattern) {
        if (origin == null || splitPattern == null) {
            return Span.ofNull();
        }
        String[] kv = origin.trim().split(splitPattern);
        if (kv.length > 2) {
            int idx = kv[0].length();
            int len = UtilRegexp.match(splitPattern, origin).get(0).length();
            return Span.of(origin.substring(0, idx), origin.substring(idx + len));
        } else if (kv.length == 2) {
            return Span.of(kv[0], kv[1]);
        } else if (kv.length == 1) {
            return Span.of(kv[0], null);
        }
        return Span.ofNull();
    }
    
    public Span<Integer, Integer> toInt() {
        return Span.of(Integer.parseInt(k.toString()), Integer.parseInt((String) v));
    }
    public Span<Long, Long> toLong() {
        return Span.of(Long.parseLong(k.toString()), Long.parseLong((String) v));
    }
    
    public boolean anyEmpty() {
        return k == null || v == null;
    }
    public boolean empty() {
        return k == null && v == null;
    }
    
    public K getK() {
        return k;
    }
    
    public V getV() {
        return v;
    }
    
    @Override
    public String toString() {
        return "(" + k + ", " + v + ")";
    }
}
