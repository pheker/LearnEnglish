package cn.pheker.learnenglish.lookingforwords.difficulty;

/**
 * <pre>
 * @author cn.pheker
 * @date 2020/5/4 19:59
 * @email 1176479642@qq.com
 * @version v1.0.0
 * @desc 单词策略
 *
 * </pre>
 */
public interface WordStrategy extends DifficultyStrategy, SearchStrategy {
}
