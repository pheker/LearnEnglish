package cn.pheker.learnenglish.lookingforwords.util;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author  cn.pheker
 * @date    2017/11/14  10:20
 * @email   1176479642@qq.com
 * @desc    正则工具:匹配,包含,验证等
 */
public class UtilRegexp {
    
    public static final String REGEXP_WORD = "\\w+";
    public static final String REGEXP_NUMBER = "\\d+";
    public static final String REGEXP_LOWER_CASE = "[a-z]+";
    public static final String REGEXP_UPPER_CASE = "[A-Z]+";
    public static final String REGEXP_LETTER = "[a-zA-Z]+";
    public static final String REGEXP_SPECIAL = "[^a-zA-Z0-9\\s]+";
    public static final String REGEXP_CHINESE = "[\\u4e00-\\u9fa5]+";
    public static final String REGEXP_BLANK = "\\s+";
    public static final String REGEXP_EMAIL = "[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+";
    public static final String REGEXP_TELEPHONE = "(0\\d{2}-\\d{8}(-\\d{1,4})?)|(0\\d{3}-\\d{7,8}(-\\d{1,4})?)";
    public static final String REGEXP_CELLPHONE = "((13[0-9])|(14[5|7])|(15([0-3]|[5-9]))|(18[0,5-9]))\\d{8}";
    public static final String REGEXP_MISS_MATCH_EXCEPTION = "([^.']+)'+\\s+to\\s+required\\s+type\\s+'[^']*?([^.']+)'";
    
    public static final String REGEXP_DATE_SIMPLE = "\\d{4}([|/\\\\.-])\\d{1,2}\\1\\d{1,2}";
    public static final String REGEXP_TIME_SIMPLE = "\\d{1,2}:\\d{1,2}(?::\\d{1,2})?";
    public static final String REGEXP_TIME_COMPLEX = "(2[0-3]|[0-1]\\d):[0-5]\\d(?::[0-5]\\d)?";
    public static final String REGEXP_DATETIME_SIMPLE = "\\d{4}([\\s/\\\\.-])\\d{1,2}\\1\\d{1,2}\\s+(2[0-3]|[0-1]\\d):[0-5]\\d(?::[0-5]\\d)?";
    
    public static final Pattern PATTERN_TAG = Pattern.compile("<(\\w+)\\s*([^<>]*)>([^<>]*)");
    public static final Pattern PATTERN_WORD = Pattern.compile("\\w+");
    public static final Pattern PATTERN_WORD_ENGLISH = Pattern.compile("[_A-Za-z]+");
    
    
    /**
     * null或空字符
     * @param string
     * @return
     */
    public static boolean isNullOrEmpty(String string) {
        return string == null||"".equals(string);
    }
    
    /**
     * 全部是单词
     * @param string
     * @return
     */
    public static boolean isWord(String string) {
        return Pattern.matches(REGEXP_WORD, string);
    }
    
    /**
     * 包含单词
     * @param string
     * @return
     */
    public static boolean containWord(String string) {
        return Pattern.compile(REGEXP_WORD).matcher(string).find();
    }
    
    /**
     * 全部是数字
     * @param string
     * @return
     */
    public static boolean isNumbers(String string) {
        return Pattern.matches(REGEXP_NUMBER, string);
    }
    
    /**
     * 包含数字
     * @param string
     * @return
     */
    public static boolean containNumbers(String string) {
        return Pattern.compile(REGEXP_NUMBER).matcher(string).find();
    }
    
    /**
     * 全部是小写字母
     * @param string
     * @return
     */
    public static boolean isLowerCase(String string) {
        return Pattern.matches(REGEXP_LOWER_CASE, string);
    }
    
    /**
     * 包含小写字母
     * @param string
     * @return
     */
    public static boolean containLowerCase(String string) {
        return Pattern.compile(REGEXP_LOWER_CASE).matcher(string).find();
    }
    
    /**
     * 全部是大写字母
     * @param string
     * @return
     */
    public static boolean isUpperCase(String string) {
        return Pattern.matches(REGEXP_UPPER_CASE, string);
    }
    
    /**
     * 包含大写字母
     * @param string
     * @return
     */
    public static boolean containUpperCase(String string) {
        return Pattern.compile(REGEXP_UPPER_CASE).matcher(string).find();
    }
        /**
     * 全部是字母
     * @param string
     * @return
     */
    public static boolean isLetter(String string) {
        return Pattern.matches(REGEXP_LETTER, string);
    }
    
    /**
     * 包含字母
     * @param string
     * @return
     */
    public static boolean containLetter(String string) {
        return Pattern.compile(REGEXP_LETTER).matcher(string).find();
    }
    
    
    /**
     * 全部是中文
     * @param string
     * @return
     */
    public static boolean isChinese(String string) {
        return Pattern.matches(REGEXP_CHINESE,string);
    }
    
    /**
     * 包含中文
     * @param string
     * @return
     */
    public static boolean containChinese(String string) {
        return Pattern.compile(REGEXP_CHINESE).matcher(string).find();
    }
    
    /**
     * 全部是特殊字符(不包括空白字符)
     * @param string
     * @return
     */
    public static boolean isSpecials(String string) {
        return Pattern.matches(REGEXP_SPECIAL,string);
    }
    
    /**
     * 包含特殊字符(不包括空白字符)
     * @param string
     * @return
     */
    public static boolean containSpecials(String string) {
        return Pattern.compile(REGEXP_SPECIAL).matcher(string).find();
    }
    
    
    
    /**
     * 全部是空白字符
     * @param string
     * @return
     */
    public static boolean isBlanks(String string) {
        return Pattern.matches(REGEXP_BLANK, string);
    }
    
    /**
     * 包含空白字符
     * @param string
     * @return
     */
    public static boolean containBlanks(String string) {
        return Pattern.compile(REGEXP_BLANK).matcher(string).find();
    }
    
    /**
     * 密码必须包含数字和字母
     * @param pwd
     * @param min   最小位数
     * @param max   最大位数
     * @return
     */
    public static boolean checkPwdSimple(String pwd,int min,int max) {
        min = min<0?0:min;
        max = max<0?0:max;
        String limitBit;
        if (max == 0) {
            limitBit = "{"+min+",}";
            if (min == 0) {
                limitBit = "+";
            }
        }else{
            if(max<2||max<min){
                return false;
            }else{
                limitBit = "{"+min+","+max+"}";
            }
        }
        return Pattern.matches("^(?=.*?[a-zA-Z]+)(?=.*?\\d+)[a-zA-Z0-9]"+limitBit+"$", pwd);
    }
    
    /**
     * 必须包含数字和字母,并且位数不做限制
     * @param pwd
     * @return
     */
    public static boolean checkPwdSimple(String pwd) {
        return checkPwdSimple(pwd, 0, 0);
    }
    
    /**
     * 必须包含数字、字母和特殊字符
     * @param pwd
     * @param min   最小位数
     * @param max   最大位数
     * @return
     */
    public static boolean checkPwdComplex(String pwd,int min,int max) {
        min = min<0?0:min;
        max = max<0?0:max;
        String limitBit;
        if (max == 0) {
            limitBit = "{"+min+",}";
            if (min == 0) {
                limitBit = "+";
            }
        }else{
            if(max<3||max<min){
                return false;
            }else{
                limitBit = "{"+min+","+max+"}";
            }
        }
        return Pattern.matches("^(?=.*?[a-zA-Z]+)(?=.*?\\d+)(?=.*?[^a-zA-Z0-9]+)" +
                "[a-zA-Z0-9!@#$%^&*=+_-`~/?,.<>;:'\"\\[\\]{}|\\\\]"+limitBit+"$", pwd);
    }
    
    /**
     * 必须包含数字、字母和特殊字符,并且位数不做限制
     * @param pwd
     * @return
     */
    public static boolean checkPwdComplex(String pwd) {
        return checkPwdComplex(pwd, 0, 0);
    }
    
    /**
     * 验证邮箱
      * @param email
     * @return
     */
    public static boolean checkEmail(String email) {
        return Pattern.matches(REGEXP_EMAIL, email);
    }
    
    /**
     * 验证手机号码
     *
     * 移动号码段:139、138、137、136、135、134、150、151、152、157、158、159、182、183、187、188、147
     * 联通号码段:130、131、132、136、185、186、145
     * 电信号码段:133、153、180、189
     *
     * @param cellphone
     * @return
     */
    public static boolean checkCellphone(String cellphone) {
        return Pattern.matches(REGEXP_CELLPHONE,cellphone);
    }
    
    /**
     * 验证固话号码
     *
     * @param telephone
     * @return
     */
    public static boolean checkTelephone(String telephone) {
        return Pattern.matches(REGEXP_TELEPHONE, telephone);
    }
    
    
    /**
     * 简单判断时间格式HH:mm:ss或HH:mm
     */
    public static boolean isTimeSimple(String time) {
        return Pattern.matches(REGEXP_TIME_SIMPLE, time);
    }
    
    /**
     * 严格判断时间格式HH:mm:ss或HH:mm
     */
    public static boolean isTimeComplex(String time) {
        return Pattern.matches(REGEXP_TIME_COMPLEX, time);
    }
    
    /**
     * 简单判断日期格yyyy/MM/dd
     */
    public static boolean isDateSimple(String date) {
        return Pattern.matches(REGEXP_DATE_SIMPLE, date);
    }
    
    /**
     * 判断日期格式和范围，分隔符为-/或空格
     */
    public static boolean isDate(String date) {
        String rexp = "^((\\d{2}(([02468][048])|([1579][26]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])))))|(\\d{2}(([02468][1235679])|([13579][01345789]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))";
        Pattern pat = Pattern.compile(rexp);
        Matcher mat = pat.matcher(date);
        boolean dateType = mat.matches();
        return dateType;
    }
    
    /**
     * 简单判断日期和时间格式
     * yyyy/MM/dd HH:mm:ss或yyyy/MM/dd HH:mm
     * 日期分隔符为/\.或空格
     */
    public static boolean isDateTimeSimple(String date) {
        return Pattern.matches(REGEXP_DATETIME_SIMPLE, date);
    }
    
    
    ///////////////////////////////////////////////////////
    //                      匹配获取指定文本                //
    ///////////////////////////////////////////////////////
    
    /**
     * 匹配标签，并获取标签名，属性，及内容
     * @param tag
     * @return
     */
    public static Map<String, Object> getSingleTag(String tag){
        Map<String, Object> retMap = new HashMap<>();
        Matcher m = PATTERN_TAG.matcher(tag);
        if (m.find()) {
            int count = m.groupCount();
            retMap.put("text",m.group());
            if (count > 0) {
                retMap.put("name", m.group(1));
            }
            if(count > 1){
                Map<String, String> attrs = new HashMap<>();
                String attrsTxt = m.group(2);
                attrsTxt = attrsTxt==null?"":attrsTxt.trim();
                if (!UtilRegexp.isBlanks(attrsTxt)) {
                    String[] attrsTmp = attrsTxt.split("\\s+");
                    for (String attrTmp : attrsTmp) {
                        String[] kv = attrTmp.split("=");
                        if (kv.length > 0) {
                            attrs.put(kv[0], kv.length>=2?kv[1]:"");
                        }
                    }
                }
                retMap.put("attrs", attrs);
            }
            if(count > 2) {
                retMap.put("content", m.group(3));
            }
        }
        return retMap;
    }
    
    /**
     * 返回所有匹配结果
     * @param regexp
     * @param src
     * @return
     */
    public static List<String> match(String regexp, String src) {
        List<String> list = new ArrayList<String>();
        Matcher m = Pattern.compile(regexp, Pattern.MULTILINE).matcher(src);
        while (m.find()) {
            list.add(m.group());
        }
        return list;
    }
    
    /**
     * 返回所有匹配结果
     * @param regexp
     * @param src
     * @return
     */
    public static String matchFirst(String regexp, String src) {
        Matcher m = Pattern.compile(regexp, Pattern.MULTILINE).matcher(src);
        if (m.find()) {
            return m.group();
        }
        return null;
    }
    
    
    /**
     * 返回所有匹配的捕获组
     * @param regexp
     * @param src
     * @param group0 包含自身group0
     * @return
     */
    public static List<String[]> matchGroups(String regexp, String src, boolean group0) {
        List<String[]> list = new ArrayList<String[]>();
        Matcher m = Pattern.compile(regexp, Pattern.MULTILINE).matcher(src);
        while (m.find()) {
            int cnt = m.groupCount();
            int len = group0?cnt+1:cnt;
            String[] groups = new String[len];
            for (int i = 1; i <= cnt; i++) {
                groups[group0?i:i-1] = m.group(i);
            }
            list.add(groups);
        }
        return list;
    }
    
    /**
     * 返回所有匹配的捕获组
     * @param regexp
     * @param src
     * @param group0 包含自身group0
     * @return
     */
    public static String[] matchGroupFirst(String regexp, String src, boolean group0) {
        Matcher m = Pattern.compile(regexp, Pattern.MULTILINE).matcher(src);
        if (m.find()) {
            int cnt = m.groupCount();
            int len = group0?cnt+1:cnt;
            String[] groups = new String[len];
            for (int i = 1; i <= cnt; i++) {
                groups[group0 ? i : i - 1] = m.group(i);
            }
            return groups;
        }
        return null;
    }
    
    
    
}
