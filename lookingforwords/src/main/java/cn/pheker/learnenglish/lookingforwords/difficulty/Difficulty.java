package cn.pheker.learnenglish.lookingforwords.difficulty;

/**
 * <pre>
 * @author cn.pheker
 * @date 2020/5/4 12:29
 * @email 1176479642@qq.com
 * @version v1.0.0
 * @desc 难度
 *
 * </pre>
 */
public class Difficulty implements Comparable{
    /**
     * 难度得分 如1.0
     */
    private Double score;
    
    /**
     * 难度级别 如S+
     */
    private String level;
    
    public static Difficulty of(Double score) {
        return new Difficulty(score, "");
    }
    
    public static Difficulty of(Double score, String name) {
        return new Difficulty(score, name);
    }
    
    public Difficulty(Double score, String level) {
        this.score = score;
        this.level = level;
    }
    
    public Double getScore() {
        return score;
    }
    
    public String getLevel() {
        return level;
    }
    
    @Override
    public String toString() {
        return "("+ score + ","+ level + ")";
    }
    
    @Override
    public int compareTo(Object o) {
        if (o instanceof Difficulty) {
            return (int) ((score - ((Difficulty) o).getScore()) * 100);
        }
        return 0;
    }
}
