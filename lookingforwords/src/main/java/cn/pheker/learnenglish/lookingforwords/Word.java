package cn.pheker.learnenglish.lookingforwords;

import cn.pheker.learnenglish.lookingforwords.common.WordNode;
import lombok.Data;

import java.util.Comparator;
import java.util.Objects;

/**
 * @author cn.pheker
 * @date        2019/4/7 14:29
 * @version     v1.0.0
 * @Description 单词信息--实际是对信息结点的包装
 */
@Data
public class Word {
    
    public boolean isNull() {
        return word == null;
    }
    
    public boolean isNotNull() {
        return word != null;
    }
    
    public boolean isEmpty() {
        return word == null || word.length() == 0;
    }
    
    public boolean isNotEmpty() {
        return !isEmpty();
    }
    /**
     * 单词
     */
    private String word;
    /**
     * 单词信息: 节点树中的最后一个节点
     * 如： 中->
     *        国->
     *           人
     *  “人”节点的信息
     */
    private transient WordNode wordNode;
    
    
    /**
     * 单词字母与长度 比较器
     */
    public static final Comparator<Word> WordComparator =
            (Word o1, Word o2) -> {
                int len = o1.word.length(), len2 = o2.word.length();
                if (len2 > len) {
                    return o1.word.compareTo(o2.word.substring(0, len));
                } else if (len2 < len) {
                    return o1.word.substring(0, len2).compareTo(o2.word);
                }
                return o1.word.compareTo(o2.word);
            };
    
    /**
     * 词频单词比较器
     */
    public static final Comparator<Word> CountWordComparator =
            (Word o1, Word o2) -> {
                int i = o2.getCount() - o1.getCount();
                if (i != 0) {
                    return i;
                }
                int len = o1.word.length(), len2 = o2.word.length();
                if (len2 > len) {
                    return o1.word.compareTo(o2.word.substring(0, len));
                } else if (len2 < len) {
                    return o1.word.substring(0, len2).compareTo(o2.word);
                }
                return o1.word.compareTo(o2.word);
            };
    /**
     * 词频比较器
     */
    public static final Comparator<Word> CountComparator =
            (Word o1, Word o2) -> o2.getCount() - o1.getCount();
    
    /**
     * 难度比较器
     */
    public static final Comparator<Word> DifficultyComparator =
            (Word o1, Word o2) -> o2.wordNode.getDifficulty().compareTo(o1.wordNode.getDifficulty());
    
    /**
     * 难度词频比较器
     */
    public static final Comparator<Word> DifficultyCountComparator =
            (Word o1, Word o2) -> {
                int cmp = o2.wordNode.getDifficulty().compareTo(o1.wordNode.getDifficulty());
                if (cmp != 0) {
                    return cmp;
                }
                return o2.getCount() - o1.getCount();
            };
        
   public static Word parseWord(WordNode wordNode){
       assert wordNode != null;
       return new Word(wordNode.getWord(), wordNode);
   }
    
    public Word(String word, WordNode wordNode) {
        this.word = word;
        this.wordNode = wordNode;
    }
    
    public int getCount() {
        return wordNode.getCount();
    }
    
    public int getDeep(){
        return wordNode.getDeep();
    }
    
    public String getWord(){
        return word;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Word word = (Word) o;
        return this.word.equals(word.word);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(word);
    }
    
    @Override
    public String toString() {
        return word;
    }
    
    public String toStringFormat() {
        return String.format("%-20s\t% 4d\t%.2f\t%s", word, getCount(),
                wordNode.getDifficulty().getScore(), wordNode.getDifficulty().getLevel());
    }
    
    public String toJson() {
        return String.format(
                "{" +
                    "\"word\":%s" +
                    "\"count\":%d, " +
                    "\"difficulty\":{" +
                        "\"score\": %.2f, " +
                        "\"level\": \"%s\"" +
                    "}" +
                "}",
                
                "\""+word+"\", ",
                getCount(),
                wordNode.getDifficulty().getScore(),
                wordNode.getDifficulty().getLevel());
    }
    
    public String toJsonFormat() {
        return String.format(
                "{" +
                        "\"word\":%-20s" +
                        "\"count\":%- 4d, " +
                        "\"difficulty\":{" +
                        "\"score\": %.2f, " +
                        "\"level\": \"%s\"" +
                        "}" +
                        "}",
                
                "\""+word+"\", ",
                getCount(),
                wordNode.getDifficulty().getScore(),
                wordNode.getDifficulty().getLevel());
    }
    
}
