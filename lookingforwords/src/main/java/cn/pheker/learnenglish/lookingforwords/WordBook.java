package cn.pheker.learnenglish.lookingforwords;

import cn.pheker.learnenglish.lookingforwords.common.WordNode;
import cn.pheker.learnenglish.lookingforwords.common.Node;
import cn.pheker.learnenglish.lookingforwords.difficulty.*;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * @author      cn.pheker
 * @date        2019/4/7 14:14
 * @version     v1.0.0
 * @Description 单词统计
 */
@Slf4j
@Data
public class WordBook implements Book {
    
    private Node root = new Node(this, Node.ROOT_CODE);
    private int size;
    private int maxDeep;
    private transient WordStrategy strategy = new EnglishStrategy();
    
    public void incrSize() {
        size++;
    }
    
    public void updateMaxDeep(int wordLen) {
        if (wordLen > maxDeep) {
            maxDeep = wordLen;
        }
    }
    
    public static WordBook build(WordStrategy strategy, String... words) {
        WordBook wordBook = new WordBook();
        wordBook.strategy = strategy;
        int len;
        if (words != null && (len = words.length) > 0) {
            for (int i = 0; i < len; i++) {
                wordBook.add(words[i]);
            }
        }
        return wordBook;
    }
    public static WordBook build(String... words) {
        WordBook wordBook = new WordBook();
        int len;
        if (words != null && (len = words.length) > 0) {
            for (int i = 0; i < len; i++) {
                wordBook.add(words[i]);
            }
        }
        return wordBook;
    }
    
    @Override
    public Word add(String word) {
        if (word == null || word.length() == 0) {
            return null;
        }
        return root.parseWord(word);
    }
    
    
    @Override
    public List<Word> list() {
        return searchWithPrefix("");
    }
    
    @Override
    public void list(int sizePer, Consumer<List<Word>> consumer) {
        searchWithPrefix("", sizePer, consumer);
    }
    
    @Override
    public void searchWithPrefix(String prefix, int sizePer, Consumer<List<Word>> consumer) {
        sizePer = sizePer < 2? 2:sizePer;
        final List<Word>[] words = new List[]{new ArrayList<Word>(sizePer)};
        int finalSizePer = sizePer;
        searchWithPrefix(prefix, word -> {
            words[0].add(word);
            if (words[0].size() > finalSizePer) {
                consumer.accept(words[0]);
                words[0] = new ArrayList<Word>(finalSizePer);
            }
        });
        if (!words[0].isEmpty()) {
            consumer.accept(words[0]);
        }
    }
    
    @Override
    public List<Word> searchWithPrefix(String prefix) {
        List<Word> words = new ArrayList<Word>();
        searchWithPrefix(prefix, word -> words.add(word));
        return words;
    }
    
    @Override
    public boolean contains(String prefix) {
        Node prefixNode = findPrefixNode(prefix);
        return prefixNode != null;
    }
    
    @Override
    public Word search(String word) {
        Node prefixNode = findPrefixNode(word);
        if (prefixNode != null && prefixNode.isWordNode()) {
            return Word.parseWord(((WordNode) prefixNode));
        }
        return null;
    }
    
    @Override
    public void searchWithPrefix(String prefix, Consumer<Word> consumer) {
        // root or node or null
        Node prefixNode = findPrefixNode(prefix);
        if(prefixNode != null) {
            if(!prefixNode.isRoot()) {
                childrenOf(prefixNode, consumer);
            }else{
                prefixNode.getChildren().values().forEach(node -> {
                    childrenOf(node, consumer);
                });
            }
        }
    }
    
    /**
     * 查找某节点下所有InfoNode 并转换为 WordInfo
     * @param prefixNode
     * @param consumer
     */
    private void childrenOf(Node prefixNode, Consumer<Word> consumer) {
        if(!prefixNode.hasChildren()){
            return;
        }
        prefixNode.getChildren().forEach((code, currNode) -> {
            if (currNode.isWordNode()) {
                consumer.accept(Word.parseWord((WordNode) currNode));
            }
            //递归
            if(currNode.hasChildren()){
                childrenOf(currNode, consumer);
            }
        });
    }
    
    private Node findPrefixNode(String prefix) {
        int i=0, len;
        if (prefix == null || (len = prefix.trim().length()) == 0) {
            return root;
        }
        Node prefixNode = root.findPrefixNode(prefix, len, i);
        return prefixNode;
    }
    
    @Override
    public int size() {
        return size;
    }
    
    public WordStrategy getStrategy() {
        return strategy;
    }
    
    
    
    
    





}
