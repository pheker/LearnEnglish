package cn.pheker.learnenglish.lookingforwords.difficulty;

import cn.pheker.learnenglish.lookingforwords.util.UtilFile;
import cn.pheker.learnenglish.lookingforwords.util.UtilRegexp;
import com.huaban.analysis.jieba.JiebaSegmenter;
import com.huaban.analysis.jieba.SegToken;

import java.io.File;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <pre>
 * @author cn.pheker
 * @date 2020/5/4 12:38
 * @email 1176479642@qq.com
 * @version v1.0.0
 * @desc 中文单词难度策略
 *
 * </pre>
 */
public class ChineseWubiStrategy implements WordStrategy {
    private JiebaSegmenter jieba = new JiebaSegmenter();
    private static Map<String, String> wubiMap = new HashMap<>();;
    
    // 加载中文单字、词组与五笔对应关系
    static {
        File file = new File("src/main/resources/wubi86.txt");
        List<String> wubi = UtilFile.readLogByList(file.getAbsolutePath());
        wubi.forEach(line -> {
            String[] cols = line.split("[ \t]+");
            int len = cols.length;
            for (int i = 1; i < len; i++) {
                wubiMap.put(cols[i], cols[0]);
            }
        });
    }
    
    /**
     * 难度级别名称, 共分九级：F最低，SSS最高
     */
    List<String> level = Arrays.asList("F", "E", "D", "C", "B", "A", "S", "SS", "SSS");
    
    @Override
    public Difficulty evaluate(String word) {
        Double score = getSumScore(word);
        int levelIdx = (int) (score*10) -1;
        levelIdx = levelIdx < 0 ? 0 : levelIdx > 8 ? 8 : levelIdx;
        return Difficulty.of(score, level.get(levelIdx));
    }
    
    public static final List<String> score5 = Arrays.asList("Q","A","Z","P","'");
    public static final List<String> score4 = Arrays.asList("W","S","X","O","L");
    public static final List<String> score3 = Arrays.asList("E","D","C","I","K");
    public static final List<String> score2 = Arrays.asList("T","G","B","Y","H","N");
    public static final List<String> score1 = Arrays.asList("R","F","V","Y","H","M");
    
    public static Double getSingleScore(char ch){
        String c = (ch+"").toUpperCase();
        Double score = 0d;
        if (score5.contains(c)) {
            score = 5.0;
        }else if (score4.contains(c)) {
            score = 4.0;
        }else if (score3.contains(c)) {
            score =  3.0;
        }else if (score2.contains(c)) {
            score =  2.0;
        }else if (score1.contains(c)) {
            score =  1.0;
        }
        if (Character.isUpperCase(ch)) {
            score++;
        }
        return score;
    }
    
    /**
     * 计算五笔对应的得分： 如 中国的五笔khlg对应的得分(字母的平均分)
     * @param wubiLetter
     * @return
     */
    public static Double getWubiLetterScore(String wubiLetter) {
        double sum = 0.0;
        int len = wubiLetter.length();
        for (int i = 0; i < len; i++) {
            sum += getSingleScore(wubiLetter.charAt(i));
        }
        return sum;
    }
    
    public static Double getSumScore(String word) {
        double sum = 0.0;
        String wubi = wubiMap.get(word);
        int len = 4;
        // 查找到词组对应的五笔
        if(!UtilRegexp.isNullOrEmpty(wubi)){
            sum = getWubiLetterScore(wubi);
        }else{
            // 找不到词组对应的五笔，就找每个字对应的五笔
            len = word.length();
            for (int i = 0; i < len; i++) {
                String ch = word.substring(i, i+1);
                wubi = wubiMap.get(ch);
                if(!UtilRegexp.isNullOrEmpty(wubi)){
                    sum += getWubiLetterScore(wubi)/len;
                }
            }
        }
        BigDecimal bigDecimal = new BigDecimal(sum/(6.0*len));
        return bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }
    
    @Override
    public List<String> search(String str) {
        List<String> sentences = UtilRegexp.match("[\\u4e00-\\u9fa5]+", str);
        return sentences.stream()
                .flatMap(sentence -> jieba.sentenceProcess(sentence).stream())
                .collect(Collectors.toList());
    }
}
