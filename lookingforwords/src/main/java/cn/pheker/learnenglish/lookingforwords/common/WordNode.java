package cn.pheker.learnenglish.lookingforwords.common;

import cn.pheker.learnenglish.lookingforwords.Word;
import cn.pheker.learnenglish.lookingforwords.WordBook;
import cn.pheker.learnenglish.lookingforwords.difficulty.Difficulty;
import lombok.Data;

/**
 * <pre>
 * @author cn.pheker
 * @date 2020/5/23 20:55
 * @email 1176479642@qq.com
 * @version v1.0.0
 * @desc 单词节点
 *
 * </pre>
 */

/**
 * 信息节点
 * @author Administrator
 */
@Data
public class WordNode extends Node {
    /**
     * 单词出现次数
     */
    private int count;
    /**
     * 单词难度
     */
    private Difficulty difficulty;
    
    public WordNode(WordBook wordBook, int code, int deep, Node pNode) {
        super(wordBook, code, deep, pNode);
        this.difficulty = wordBook.getStrategy().evaluate(Word.parseWord(this).getWord());
    }
    
    @Override
    public String toString() {
        return String.format("{\"code\":\"%s\", \"deep\":%d, \"count\":%d}",
                (char)code, deep, count);
    }
    
    public int incrCount() {
        return ++count;
    }
    
}