import cn.pheker.learnenglish.lookingforwords.util.UtilFile;
import cn.pheker.learnenglish.lookingforwords.util.UtilRegexp;
import com.huaban.analysis.jieba.JiebaSegmenter;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <pre>
 * @author cn.pheker
 * @date 2020/5/4 21:13
 * @email 1176479642@qq.com
 * @version v1.0.0
 * @desc TODO write your function introduction
 *
 * </pre>
 */
@Slf4j
public class JiebaTest {
    private JiebaSegmenter jieba = new JiebaSegmenter();
    
    /**
     * 测试分词
     */
    @Test
    public void testJieba() {
        String str = UtilFile.readLogByString("C:\\Users\\Administrator\\Desktop\\高频词汇.txt");
        List<String> sentences = UtilRegexp.match("[\\u4e00-\\u9fa5]+", str);
        List<String> words = sentences.stream()
                .flatMap(sentence -> jieba.sentenceProcess(sentence).stream())
                .collect(Collectors.toList());
        log.info("words: {}", words);
    }
}
