import cn.pheker.learnenglish.lookingforwords.Book;
import cn.pheker.learnenglish.lookingforwords.WordBook;
import cn.pheker.learnenglish.lookingforwords.Word;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.List;

/**
 * author    version  date
 * ---------------------------------
 * cn.pheker v1.0.0   2019/4/7 17:25
 *
 * @Description WordBook测试
 */

@Slf4j
public class WordBookTest {
    
    @Test
    public void test() {
        Book book = WordBook.build();
        book.add("中国");
        book.add("中国人");
        book.add("中国人");
        book.add("中心");
        book.add("AnJie vs 李文静");
        int size = book.size();
        log.info("size: {}", size);
        List<Word> all = book.list();
        log.info("book: {}", all);
    }

}
