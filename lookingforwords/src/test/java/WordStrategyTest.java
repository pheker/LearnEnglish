import cn.pheker.learnenglish.lookingforwords.difficulty.ChineseWubiStrategy;
import cn.pheker.learnenglish.lookingforwords.difficulty.Difficulty;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * <pre>
 * @author cn.pheker
 * @date 2020/5/4 19:24
 * @email 1176479642@qq.com
 * @version v1.0.0
 * @desc TODO write your function introduction
 *
 * </pre>
 */
@Slf4j
public class WordStrategyTest {
    
    @Test
    public void testWubi() {
        ChineseWubiStrategy wubiStrategy = new ChineseWubiStrategy();
        Difficulty dif = wubiStrategy.evaluate("模式");
        log.info("dif: {}", dif);
        Difficulty dif2 = wubiStrategy.evaluate("模式模式");
        log.info("dif2: {}", dif2);
    }
}
