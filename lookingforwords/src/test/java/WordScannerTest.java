import cn.pheker.learnenglish.lookingforwords.Word;
import cn.pheker.learnenglish.lookingforwords.WordBook;
import cn.pheker.learnenglish.lookingforwords.WordScanner;
import cn.pheker.learnenglish.lookingforwords.difficulty.ChineseWubiStrategy;
import cn.pheker.learnenglish.lookingforwords.util.StopWatch;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <pre>
 * @author cn.pheker
 * @date 2020/5/4 15:05
 * @email 1176479642@qq.com
 * @version v1.0.0
 * @desc 测试单词扫描器
 *
 * </pre>
 */

@Slf4j
public class WordScannerTest {
    
    @Test
    public void testAddAndSearch() {
        WordBook wordBook = WordBook.build("word", "wordBook", "jack", "jack ma");
        Word word = wordBook.search("word");
        List<Word> wor = wordBook.searchWithPrefix("wor");
        List<Word> words = wordBook.list();
        List<String> all = words.stream()
                .map(Word::getWord)
                .collect(Collectors.toList());
        log.info("all: {}", all);
    }
    
    @Test
    public void scanSingleText() {
        StopWatch sw = new StopWatch();
        
        sw.start();
        String folder = "D:\\IntelliJ\\workspace\\LearnEnglish\\lookingforwords\\src\\main\\java\\cn\\pheker" +
                "\\learnenglish\\lookingforwords\\WordBook.java";
        WordScanner.scanAndSave(folder);
        sw.stop();
        
        String s = sw.prettyPrint();
        System.out.println(s);
    }
    
    @Test
    public void scanFolder() {
        StopWatch sw = new StopWatch();
        
        sw.start();
        String folder = "D:\\IntelliJ/workspace/LearnEnglish";
        WordScanner.scanAndSave(folder);
        sw.stop();
    
        String s = sw.prettyPrint();
        System.out.println(s);
    }
    
    @Test
    public void scanCustom() {
        StopWatch sw = new StopWatch();
        
        sw.start();
        WordScanner wordScanner = WordScanner.Builder()
                .withRootDir("C:\\Users\\Administrator\\Desktop")
                .withAddExtraScanFiles("D:\\我的文档\\GitHub", "D:\\IntelliJ/workspace")
                .withDefaultComparator(Word.CountWordComparator)
                .build();
        wordScanner.scan()
                .Saver().withFileWillSave("EnglishCountWordWordList.txt")
                .save();
        sw.stop();
        
        String s = sw.prettyPrint();
        System.out.println(s);
    }
    
    @Test
    public void scanCustomManyFile() {
        StopWatch sw = new StopWatch();
        
        sw.start();
        WordScanner wordScanner = WordScanner.Builder()
                .withRootDir("D:\\IntelliJ/")
                .withDefaultComparator(Word.DifficultyCountComparator)
                .withAddExtraScanFiles("D:\\IntelliJ/", "D:\\我的文档\\GitHub")
                .withFileWillSave("C:\\Users\\Administrator\\Desktop/EnglishDifficultyCountWordList.txt")
                .build();
        wordScanner.scan().save();
        sw.stop();
        
        String s = sw.prettyPrint();
        System.out.println(s);
    }
    
    
    @Test
    public void testWubi() {
        StopWatch sw = new StopWatch();
    
        // scan
        sw.start();
        String folder = "C:\\Users\\Administrator\\Desktop\\常用.txt";
        WordScanner wordScanner = WordScanner.Builder()
                .withRootDir(folder)
                .withFileWillSave("WubiDifficultyWordList.txt")
                .withFileFilter(f -> true)
                .withWordStrategy(new ChineseWubiStrategy())
                .withDefaultComparator(Word.DifficultyCountComparator)
                .build();
        wordScanner.scan();
        sw.stop();
        
        // save
        sw.start();
        wordScanner.save();
        sw.stop();
    
        sw.start();
        wordScanner.Saver()
                .withFileWillSave("WubiCountWordList.txt")
                .withDefaultComparator(Word.CountComparator)
                .save();
        sw.stop();
    
        String s = sw.prettyPrint();
        System.out.println(s);
    }
    
}
